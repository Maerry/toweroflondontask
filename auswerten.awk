##########################################################
### PREPARATION: Initialize variables and name columns ###
##########################################################

BEGIN {
  estimated_moves = ""; make_timeout = estim_timeout = 0;
  if (h) print "SubjID session run TaskVersion TowerNumber StartTower GoalTower EstimMoves EstimTimeout MadeMoves MadeTimeout EstimStartTime EstimEndTime MakeStartTime MakeEndTime"
}

#############################################################
### 1ST STEP: Filter irrelevant rows of the raw log files ###
#############################################################

## Only keep rows that contain one of these keywords:
$5 !~ /PresInst|PresTask|ClickFailure|EndMove|TimeOut/ {next}
# For the keyword "ClickFailure", only keep rows, where the subject typed a number:
$5 == "ClickFailure" && $6 !~ /Key[[:digit:]]/ {next}

###################################################################################
### 2ND STEP: Collect relevant information of the log files, sort it, and print ###
###################################################################################

## Approach: Determine if the current task is to estimate moves (estim_state) or execute moves (make_state)
# In either state, at "PresTask", the tower problem is stated (start_tower and goal_tower) and the subject starts to work on it (estim_starttime in estim_state and make_starttime in make_state).
# In either state, when "TimeOut" happens, the subject went over the available time without finishing (estim_timeout in estim_state and make_timeout in make_state).
# In estim_state at "ClickFailure", subjects indicate how many moves they estimate they will need to solve the problem (estimated_moves).
# In make_state at "EndMove", the subject's executed moves are counted (made_moves).

$5 == "PresInst" && /Wie&viele&Zuege/ {estim_state = 1; make_state = 0}
estim_state && $5 == "PresTask" {estim_starttime = $2; start_tower = $7; goal_tower = $8}
estim_state && $5 == "ClickFailure" {estim_endtime = $2; estimated_moves = substr($6, 4, 1)}
estim_state && $5 == "TimeOut" {estim_endtime = $2; estim_timeout = 1}

$5 == "PresInst" && /Zeige&die&Loesung/ {make_state = 1; estim_state = 0}
make_state && $5 == "PresTask" {make_starttime = $2}
make_state && $5 == "EndMove" {made_moves ++}
make_state && $5 == "TimeOut" {make_timeout = 1}

## At the end of make_state (after either solving the problem or TimeOut), the subject is told "Super!" at "PresInst".
# This is the key phrase to collect make_endtime, print all collected data, and reset the variables.
# It should rather be $5 == "FinishTask", $6 == "SolutionEnding"

$5 == "PresInst" && /Super!/ && made_moves > 0 {
  make_endtime = $2;
  print ++n, start_tower, goal_tower, estimated_moves, estim_timeout, made_moves, make_timeout, estim_starttime, estim_endtime, make_starttime, make_endtime;
  made_moves = 0; estimated_moves = ""; estim_timeout = make_timeout = 0
}
