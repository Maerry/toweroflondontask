#!/bin/bash
# usage in bash: alle_auswerten.sh

echo "" | ./auswerten.sh > auswertung_alle.txt

for f in *_LogFile.tow; do
	./auswerten.sh -s < "$f" | awk -v f="$f" '{split(f, arr, "_"); print arr[1], arr[2], substr(arr[3], 1, 1), substr(arr[3], 2), $0}'
done >> auswertung_alle.txt
