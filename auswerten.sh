#!/bin/bash
# usage: auswerten.sh < input_LogFile.tow > outputfile.txt
# TDs: minimal number of moves

# Vorsicht: Texte in den .PSet Dateien dürfen nicht geändert werden ("Super!", "Wie&viele&Zuege/", "Zeige&die&Loesung/"; deshalb auch nur für die deutsche Version.)

headers=1
[[ "$1" == "-s" ]] && headers=0
awk -v h=$headers -f auswerten.awk
